var swiper = undefined;

function initSwiperCarousel() {
	var screenWidth = $(window).width();
    if(screenWidth > 768 && swiper == undefined) {            
        swiper = new Swiper('.swiper-carousel', {
			slidesPerView: 1,
			centeredSlides: true,
			spaceBetween: 0,
			loop: true,
			loopFillGroupWithBlank: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			}
		});
    } else if (screenWidth < 768 && swiper != undefined) {
        swiper.destroy();
        swiper = undefined;
        $('.swiper-wrapper').removeAttr('style');
        $('.swiper-slide').removeAttr('style');            
    }      
}

$(window).on('load resize', function() {
	initSwiperCarousel();
});